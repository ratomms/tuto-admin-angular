export class User {
    ID: number;
    Name: string;
    City: string;
    Salary: number;
    Country: string;
}