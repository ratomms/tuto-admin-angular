import { Component, OnInit } from '@angular/core';
import { TableService } from './table.service';

import { Observable } from 'rxjs/Rx';
import { concat } from 'rxjs/observable/concat';
import { User } from './user.model';

declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}

@Component({
    selector: 'table-cmp',
    moduleId: module.id,
    templateUrl: 'table.component.html'
})

export class TableComponent implements OnInit{
    public tableData1: TableData;
    public tableData2: TableData;
    public tableData3Rows : Object[];
    public myTableData : User[] = [];
    public myTableHeaders : string[];
    public hasData : boolean = false;

    constructor(private tablesvc: TableService){
    }

    ngOnInit(){
        this.tableData1 = {
            headerRow: [ 'ID', 'Name', 'Country', 'City', 'Salary'],
            dataRows: [
                ['1', 'Dakota Rice', 'Niger', 'Oud-Turnhout', '$36,738'],
                ['2', 'Minerva Hooper', 'Curaçao', 'Sinaai-Waas', '$23,789'],
                ['3', 'Sage Rodriguez', 'Netherlands', 'Baileux', '$56,142'],
                ['4', 'Philip Chaney', 'Korea, South', 'Overland Park', '$38,735'],
                ['5', 'Doris Greene', 'Malawi', 'Feldkirchen in Kärnten', '$63,542'],
                ['6', 'Mason Porter', 'Chile', 'Gloucester', '$78,615']
            ]
        };

        this.tableData2 = {
            headerRow: [ 'ID', 'Name',  'Salary', 'Country', 'City' ],
            dataRows: [
                ['1', 'Dakota Rice','$36,738', 'Niger', 'Oud-Turnhout' ],
                ['2', 'Minerva Hooper', '$23,789', 'Curaçao', 'Sinaai-Waas'],
                ['3', 'Sage Rodriguez', '$56,142', 'Netherlands', 'Baileux' ],
                ['4', 'Philip Chaney', '$38,735', 'Korea, South', 'Overland Park' ],
                ['5', 'Doris Greene', '$63,542', 'Malawi', 'Feldkirchen in Kärnten', ],
                ['6', 'Mason Porter', '$78,615', 'Chile', 'Gloucester' ]
            ]
        };

    /*  
        // For Promise

        this.tablesvc.getUsers().then(resultat => {
            console.log("New Loading data...");
            this.tableData3Rows = resultat.data;
            console.log(this.tableData3Rows);
        }); */

        /*
        this.tablesvc.getUsers(Observable.timer(0, 5000))
            .subscribe(resultat => {
                this.myTableData = resultat.data 
        });*/

        this.myTableHeaders = [ 'ID', 'Name', 'City', 'Salary', 'Country' ];

        this.initTable();

    }

    private initTable() : void {
        this.tablesvc.loadUsers(null).subscribe(data => { 
            this.myTableData = data;
            this.hasData = (this.myTableData.length>0)?true:false;
        });
    }
    
    private refreshPeriodicallyData(): void{
        
        this.tablesvc.checkDbUpdatePeriodically(Observable.timer(0, 10000)).subscribe(res => {
            if(res.status == 'true'){
                console.log("Yes, there is a data");

                let strdate = res.data.ModifDate; // On recupere la date de la dernière modification
                let fromDate = strdate.toString().replace(' ','_');

                // On va recuperer tout simplement les données mise à jour et nouvellement inseré
                this.tablesvc.loadUsers(fromDate).subscribe(data => {
                    //console.log(data);
                    if(this.myTableData.length<1) {
                        this.myTableData = data;
                    }
                    else{
                        
                        let tempTable = data as User[];

                        tempTable.forEach((item) => {
                            
                            let index = this.myTableData.findIndex(x => x.ID == item.ID);
                            if(index >= 0) {
                                this.myTableData[index].Country = item.Country;
                                this.myTableData[index].Name = item.Name;
                                this.myTableData[index].City = item.City;
                                this.myTableData[index].Salary = item.Salary;
                            }
                            else{
                                this.myTableData.push(item);
                            }
                            
                        });
                    }

                    this.hasData = (this.myTableData.length>0)?true:false;
                });
            }
            else{
                console.log("Oh, No data to load");
            }
        });
        
    }

    private refreshData(): void{
        /*
        this.tablesvc.checkDbUpdate()
            .then(this.tablesvc.loadUpdate)
            .then(res => {
                //let msg = res.data;
                //console.log('Data at  '+Date()+' : status = '+res.status+'; ModifDate = '+msg.ModifDate);
                console.log(res);
            })
            .catch( error =>  console.log(error)); 
        */
        
        this.tablesvc.checkDbUpdate().subscribe(res => {
            if(res.status == 'true'){
                console.log("Yes, there is a data");

                let strdate = res.data.ModifDate; // On recupere la date de la dernière modification
                let fromDate = strdate.toString().replace(' ','_');

                // On va recuperer tout simplement les données mise à jour et nouvellement inseré
                this.tablesvc.loadUsers(fromDate).subscribe(data => {
                    //console.log(data);
                    if(this.myTableData.length<1) {
                        this.myTableData = data;
                    }
                    else{
                        
                        let tempTable = data as User[];

                        tempTable.forEach((item) => {
                            
                            let index = this.myTableData.findIndex(x => x.ID == item.ID);
                            if(index >= 0) {
                                this.myTableData[index].Country = item.Country;
                                this.myTableData[index].Name = item.Name;
                                this.myTableData[index].City = item.City;
                                this.myTableData[index].Salary = item.Salary;
                            }
                            else{
                                this.myTableData.push(item);
                            }
                            
                        });
                    }

                    this.hasData = (this.myTableData.length>0)?true:false;
                });
            }
            else{
                console.log("Oh, No data to load");
            }
        });
        
    }
}
