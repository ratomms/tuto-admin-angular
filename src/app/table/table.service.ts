import { Injectable } from '@angular/core';
import {HttpModule, Http, Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';
//import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { resolve, reject } from 'q';

import { User } from './user.model';

@Injectable() 
export class TableService {
  baseApiURL : string = 'http://localhost/project/tuto-admin-angular-api/api';

  constructor(public http: Http) { 
    
  }

/*   getUsers(): Promise<any>{

    return this.http.get(this.apiURL)
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  } */

  /*   private handleError(error : any): Promise<any> {
      console.error('An error occured',error);

      return Promise.reject(error.message || error);
  } */
  
  getUsers(trigger: Observable<any>): Observable<any>{

    return trigger.flatMap(() => 
    this.http.get(this.baseApiURL+'/users')
        .map(res => res.json())
        .catch(this.handleError)
    );
  }

  /*
  checkDbUpdate(trigger: Observable<any>): Promise<any>{

    return this.http.get(this.apiURL)
    .toPromise()
    .then(res => res.json())
    .catch(this.handleErrorPromise);
  }
  */

/*
checkDbUpdate(trigger: Observable<any>): Promise<any>{

  let rand = Math.floor((Math.random()*6)+1);

  return new Promise(function(resolve, reject) {
                resolve(rand);
            });
  
}

loadUpdate(valeur): Promise<any>{

    return new Promise(function(resolve, reject) {
                let res = 'All updated data are loaded';

                if(valeur%2 == 0) res = 'No data loaded';
                
                resolve(res);
            }); 
}

*/
/*
  checkDbUpdate() {
  
    const promise = new Promise((resolve, reject) => {
        let resultat = {};
        this.http.get(this.baseApiURL+'/checkdb/1').subscribe(res => {
            resultat = res.json();
            resolve(resultat);
        });

      });
      
    return promise;
  }
*/
  checkDbUpdatePeriodically(trigger: Observable<any>){
      
    return trigger.flatMap(() => 
      this.http.get(this.baseApiURL+'/checkdb/1')
        .map(res => res.json())
        .catch(this.handleError)
      );
  }

  checkDbUpdate(){

    return this.http.get(this.baseApiURL+'/checkdb/1')
        .map(res => res.json())
        .catch(this.handleError);
  }

  loadUpdate(valeur) {
    
    return this.http.get(this.baseApiURL+'/users')
              .toPromise()
              .then(res => res.json())
              .catch(error => this.handleErrorPromise(error)); 

  }

  loadUsers(fromDate) {
    let url = this.baseApiURL+'/users';
    if(fromDate){
        url = this.baseApiURL+'/users/'+fromDate;
    }
    return this.http.get(url)
              .map(res => res.json().data as User[])
              .catch(error => this.handleErrorPromise(error));
  }

  fnLoadUpdate(): Promise<any>{
    
   let dbStatus;
   let users;

    return this.http.get(this.baseApiURL+'/checkdb/1')
            .toPromise()
            .then(res => {
                dbStatus = res.json();
            })
            .then(() => {

              if(dbStatus.status == 'true'){

                /*
                let users = [
                  { City: 'TANA', ID: 1, Country: 'MADA', Name: 'TEMA' },
                  { City: 'TANA', ID: 2, Country: 'MADA', Name: 'TEMA 2' },
                  { City: 'PARIS', ID: 3, Country: 'FRANCE', Name: 'TEMA 3' },
                ];
                */

                this.http.get(this.baseApiURL + '/users')
                          .map((res:Response) => res.json())
                          .subscribe(
                            data => { users = data; console.log(users); },
                            err => console.error(err)
                          );

                return users;
              }
              else{
                console.log('');
                return null;
              }
            });
            
  }


  private handleError(error : any): Observable<any> {
    console.error('An error occured',error);

    return Observable.throw(error.message || error);

  }

  private handleErrorPromise(error : any): Promise<any> {
    console.error('An error occured',error);

    return Promise.reject(error.message || error);
  }

}